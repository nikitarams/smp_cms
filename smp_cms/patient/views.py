import requests
from flask import Blueprint, jsonify, request, current_app, render_template
from flask_login import login_user, logout_user, current_user
from sqlalchemy.exc import IntegrityError
from .model import *
# from ..utils.auth import generate_token, auth_required, verify_token, access_permssion, only_su
# from ..utils.resolutions import Resolution
from smp_cms.utils.view_utils import *
from smp_cms.utils.user_utils import *
from smp_cms.utils.model_utils import get_or_create, get_or_update
from smp_cms.utils.database import db
from tests.api_test_patient import generate_patient

blueprint = Blueprint('patients', __name__)


@blueprint.route('/', methods=["POST"])
def create_patient():
    incoming = request.get_json()
    patient = Patient(
        name=incoming["name"],
        surname=incoming["surname"],
        cat=incoming["cat"],
        middlename=incoming["middlename"],
        gender=incoming["gender"],
        soc_status=incoming["soc_status"],
        snils=incoming["snils"].replace('-', '').replace(' ', ''),
        birthday=incoming["birthday"],
        deathday=deathday,
        comments=incoming["comments"],
    )
    db.session.add(patient)
    try:
        db.session.commit()
    except(IntegrityError):
        return jsonify(message="Пациент с подобными данными уже существует")
    return jsonify(patient=patient.as_dict())


@blueprint.route('/patients', methods=["GET"])
@is_api
def get_patients(is_api):
    if is_api:
        patient = Patient.query.all()
        return jsonify(patient=[i.as_dict() for i in patient])
    else:
        return render_template('/patients/index.html')


@blueprint.route('/patients', methods=["POST"])
def create():
    pass


@blueprint.route('/api/patients', methods=["POST"])
def api_create():
    incoming = request.get_json()

    if ('patient' in incoming):
        patient = get_or_create(Patient, **incoming['patient'])

    if 'policy' in incoming:
        if len(incoming['policy']) > 0:
            incoming['policy']['patient_id'] = patient.id
            get_or_create(Policy, **incoming['policy'])

    if 'document' in incoming:
        if len(incoming['document']) > 0:
            incoming['document']['patient_id'] = patient.id
            get_or_create(Document, **incoming['document'])

    return jsonify(patient=patient.as_dict())


@blueprint.route('/api/patients/<patient_id>', methods=["PUT"])
@get_patient
def api_update_patient(patient):
    incoming = request.get_json()

    if ('patient' in incoming):
        patient = get_or_update(Patient, patient, **incoming['patient'])

    if 'policy' in incoming:
        if len(incoming['policy']) > 0:
            incoming['policy']['patient_id'] = patient.id
            get_or_create(Policy, **incoming['policy'])

    if 'document' in incoming:
        if len(incoming['document']) > 0:
            incoming['document']['patient_id'] = patient.id
            get_or_create(Document, **incoming['document'])

    return jsonify(patient=patient.as_dict())


@blueprint.route('/api/smo', methods=["GET"])
def get_smo_search():
    incoming = request.args
    if incoming and 'search' in incoming:
        smo = Smo.query.filter(Smo.full_title.like("%" + incoming['search'] + "%")).all()
    else:
        smo = Smo.query.all()
    return jsonify(smo=[s.as_dict() for s in smo])


@blueprint.route('/api/smo/local', methods=["GET"])
def get_local_smo():
    smo = Smo.query.filter(Smo.id.between(current_app.config['REGION'], current_app.config['REGION'] + 999 )).all()
    return jsonify(smo=[s.as_dict() for s in smo])


@blueprint.route('/api/policy/<patient_id>', methods=["GET"])
def get_policy(patient_id):
    policy = Policy.query.filter_by(patient_id=patient_id).all()
    return jsonify(policy=[i.as_dict() for i in policy])


@blueprint.route('/api/patients/search', methods=["GET"])
def search_patient():
    from dateutil.relativedelta import relativedelta
    incoming = request.json
    flist = []
    if 'surname' in incoming:
        flist.append(Patient.surname.like(incoming['surname'] + "%"))

    if 'name' in incoming:
        flist.append(Patient.name.like(incoming['name'] + "%"))

    if 'last_name' in incoming:
        flist.append(Patient.last_name.like("%" + incoming['last_name'] + "%"))

    if 'age' in incoming:
        searchdate = datetime.now() - relativedelta(years=incoming['age'])
        flist.append(Patient.birthday.between(searchdate.date() - relativedelta(years=5), searchdate.date() + relativedelta(years=5)))

    if 'limit' in incoming:
        patients = Patient.query.filter(*flist).limit(incoming['limit'])
    else:
        patients = Patient.query.filter(*flist).all()
    return jsonify(patients=[s.as_dict() for s in patients])


@blueprint.route('/api/patients/<patient_id>', methods=["GET"])
def get_patient(patient_id):
    patient = Patient.query.filter_by(id=patient_id).first()
    return jsonify(patient=patient.as_dict())




######## TESTS
@blueprint.route('/api/fake/patients', methods=["GET"])
def generate_patients():
    res = generate_patient()
    return jsonify(res.json())
