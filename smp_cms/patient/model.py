from smp_cms.utils.database import bcrypt, db, ModelGetByIdMixin, SurrogatePK, Timestamps, ReferenceCol, Model
from sqlalchemy.orm import relationship, backref
from sqlalchemy import UniqueConstraint
from datetime import datetime
from calendar import monthrange
from smp_cms.constants import *
from smp_cms.utils.model_utils import check_snils, JsonModel
# make_versioned(user_cls=None)
from sqlalchemy import func
from sqlalchemy.ext.hybrid import hybrid_property, hybrid_method


class Smo(Model, ModelGetByIdMixin, JsonModel):
    __tablename__ = 'smos'
    nsi_file = 'F002.xml'

    id = db.Column(db.Integer(), primary_key=True)
    short_title = db.Column(db.String())
    full_title = db.Column(db.String())
    ogrn = db.Column(db.Integer())
    okato = db.Column(db.Integer())
    inn = db.Column(db.Integer())
    kpp = db.Column(db.Integer())

    @staticmethod
    def _bootstrap(item, count=1):
        tosave = DocumentType(id=item[0], title=item[1])
        db.session.add(tosave)
        try:
            db.session.commit()
        except Exception:
            db.session.rollback()


class PolicyType(Model, ModelGetByIdMixin, JsonModel):
    __tablename__ = 'policy_types'
    nsi_file = "F008.xml"

    id = db.Column(db.Integer(), primary_key=True)
    title = db.Column(db.String(100))

    # @staticmethod
    # def _bootstrap(item, count=1):
    #     tosave = DocumentType(id=item[0], title=item[1])
    #     db.session.add(tosave)
    #     try:
    #         db.session.commit()
    #     except Exception:
    #         db.session.rollback()


class DocumentType(Model, ModelGetByIdMixin, JsonModel):
    __tablename__ = 'document_types'

    id = db.Column(db.Integer(), primary_key=True)
    title = db.Column(db.String())
    prefix_mask = db.Column(db.String())
    number_mask = db.Column(db.String())

    @staticmethod
    def _bootstrap(item, count=1):
        tosave = DocumentType(id=item[0], title=item[1], prefix_mask=item[2], number_mask=item[3])
        db.session.add(tosave)
        try:
            db.session.commit()
        except Exception:
            db.session.rollback()


class Document(Model, ModelGetByIdMixin, Timestamps, SurrogatePK, JsonModel):
    __tablename__ = 'documents'

    prefix = db.Column(db.String())
    number = db.Column(db.String())
    document_type_id = ReferenceCol('document_types', index=True, nullable=True)
    document_type = relationship('DocumentType', remote_side="DocumentType.id", foreign_keys=[document_type_id])
    patient_id = ReferenceCol('patients', index=True, nullable=True)
    patient = relationship('Patient', backref=backref("document"), remote_side="Patient.id", foreign_keys=[patient_id])

    def as_dict(self):
        model = self.as_dict_glob()
        model['document_type'] = self.document_type.as_dict_glob()
        return model

class Policy(Model, ModelGetByIdMixin, SurrogatePK, Timestamps, JsonModel):
    __tablename__ = 'policies'

    prefix = db.Column(db.String())
    number = db.Column(db.String())
    period_start = db.Column(db.Date)
    period_end = db.Column(db.Date)
    patient_id = ReferenceCol('patients', index=True)
    patient = relationship('Patient', backref=backref("policy", order_by="asc(Policy.period_start)"))
    policy_type_id = ReferenceCol('policy_types', index=True, nullable=True)
    policy_type = relationship('PolicyType', remote_side="PolicyType.id", foreign_keys=[policy_type_id])
    smo_id = ReferenceCol('smos', index=True, nullable=True)
    smo = relationship('Smo', remote_side="Smo.id", foreign_keys=[smo_id])

    def as_dict(self):
        model = self.as_dict_glob()
        model['smo'] = self.smo.as_dict_glob()
        model['policy_type'] = self.policy_type.as_dict_glob()
        return model


class Patient(Model, ModelGetByIdMixin, SurrogatePK, Timestamps, JsonModel):
    __tablename__ = 'patients'
    __table_args__ = (UniqueConstraint('name', 'surname','last_name', 'birthday', 'snils', name='_unic_patient'),)
    # __versioned__ = {}

    name = db.Column(db.String())
    surname = db.Column(db.String())
    last_name = db.Column(db.String())
    gender = db.Column(db.Integer())
    snils = db.Column(db.Integer(), nullable=True)
    birthday = db.Column(db.Date)
    deathday = db.Column(db.Date)
    comments = db.Column(db.String())
    parent_id = ReferenceCol('patients', index=True, nullable=True)

    parent = relationship('Patient',
                          backref=backref('children', cascade="all,delete", order_by="desc(Patient.created_at)"),
                          remote_side="Patient.id", foreign_keys=[parent_id])


    def __init__(self, snils=None, birthday=None, deathday=None, **kwargs):
        super(Patient, self).__init__(**kwargs)

        if (birthday):
            self.birthday = datetime.strptime(birthday, '%Y-%m-%d').date()

        if snils:
            if (check_snils(snils)):
                self.snils=snils
            else:
                self.snils=0

        if bool(deathday):
            self.deathday = datetime.now().date()


    def is_alive(self, date=datetime.now()):
        if self.deathday > date:
            return True
        return False

    def is_newborn(self, date=datetime.now()):
        if self.birthday < date.timedelta(days=monthrange(date.year, date.month)[1])\
                and self.middlename == "НЕТ" and self.surname == "НЕТ":
            return self.gender + self.birthday.strftime('%d%m%y') + '01'
        return False

    def age(self):
        if not self.birthday:
            return None
        today = datetime.today()
        return today.year - self.birthday.year - ((today.month, today.day) < (self.birthday.month, self.birthday.day))



    def as_dict(self):
        model = self.as_dict_glob()
        model['age'] = self.age()
        model['children'] = [child.as_dict() for child in self.children]
        if self.parent: model['parent'] = self.parent.as_short_dict()
        model['policy'] = [police.as_dict() for police in self.policy]
        model['documents'] = [doc.as_dict() for doc in self.document]
        return model

    def as_short_dict(self):
        model = self.as_dict_glob()
        model['age'] = self.age()
        model['policy'] = [police.as_dict() for police in self.policy]
        model['documents'] = [doc.as_dict() for doc in self.document]
        return model

