# -*- coding: utf-8 -*-
"""The app module, containing the app factory function."""
from flask import Flask, render_template
from smp_cms.constants import *
from smp_cms import commands, public, user, services, call, patient
from smp_cms.extensions import bcrypt, cache, csrf_protect, db, debug_toolbar, login_manager, migrate, webpack


def create_app(config_object='smp_cms.settings'):
    """An application factory, as explained here: http://flask.pocoo.org/docs/patterns/appfactories/.

    :param config_object: The configuration object to use.
    """
    app = Flask(__name__.split('.')[0])
    app.url_map.strict_slashes = False
    app.config.from_object(config_object)
    register_extensions(app)
    register_blueprints(app)
    register_errorhandlers(app)
    register_shellcontext(app)
    register_commands(app)
    return app


def register_extensions(app):
    """Register Flask extensions."""
    bcrypt.init_app(app)
    cache.init_app(app)
    db.init_app(app)
    # csrf_protect.init_app(app)
    login_manager.init_app(app)
    debug_toolbar.init_app(app)
    migrate.init_app(app, db)
    webpack.init_app(app)
    return None


def register_blueprints(app):
    """Register Flask blueprints."""
    app.register_blueprint(public.views.blueprint)
    app.register_blueprint(services.views.blueprint)
    app.register_blueprint(call.views.blueprint)
    app.register_blueprint(user.views.blueprint)
    app.register_blueprint(patient.views.blueprint)
    return None


def register_errorhandlers(app):
    """Register error handlers."""
    def render_error(error):
        """Render error template."""
        # If a HTTPException, pull the `code` attribute; default to 500
        error_code = getattr(error, 'code', 500)
        return render_template('{0}.html'.format(error_code)), error_code
    for errcode in [401, 404, 500]:
        app.errorhandler(errcode)(render_error)
    return None


def register_shellcontext(app):
    """Register shell context objects."""
    def shell_context():
        """Shell context objects."""
        return {
            'db': db,
            'User': user.models.User}

    app.shell_context_processor(shell_context)

    @app.context_processor
    def utility_processor():
        from smp_cms.public.forms import LoginForm
        def login_form():
            return LoginForm()
        return dict(login_form=LoginForm())

    @app.context_processor
    def utility_processor():
        from flask_login import current_user
        def menus():
            return MENUS
        return dict(menus=menus(), curr_user=current_user)


def register_commands(app):
    """Register Click commands."""
    app.cli.add_command(commands.test)
    app.cli.add_command(commands.lint)
    app.cli.add_command(commands.clean)
    app.cli.add_command(commands.urls)
    app.cli.add_command(commands.init_data)
    app.cli.add_command(commands.total_delete)
    app.cli.add_command(commands.generate_patient)
