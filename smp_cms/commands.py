# -*- coding: utf-8 -*-
"""Click commands."""
import os, random
from glob import glob
from subprocess import call
from datetime import datetime, timedelta
import click
from flask import current_app
from flask.cli import with_appcontext
from werkzeug.exceptions import MethodNotAllowed, NotFound
from dateutil.relativedelta import relativedelta

HERE = os.path.abspath(os.path.dirname(__file__))
PROJECT_ROOT = os.path.join(HERE, os.pardir)
TEST_PATH = os.path.join(PROJECT_ROOT, 'tests')


@click.command()
def test():
    """Run the tests."""
    import pytest
    rv = pytest.main([TEST_PATH, '--verbose'])
    exit(rv)


@click.command()
@click.option('-f', '--fix-imports', default=False, is_flag=True,
              help='Fix imports using isort, before linting')
def lint(fix_imports):
    """Lint and check code style with flake8 and isort."""
    skip = ['node_modules', 'requirements']
    root_files = glob('*.py')
    root_directories = [
        name for name in next(os.walk('.'))[1] if not name.startswith('.')]
    files_and_directories = [
        arg for arg in root_files + root_directories if arg not in skip]

    def execute_tool(description, *args):
        """Execute a checking tool with its arguments."""
        command_line = list(args) + files_and_directories
        click.echo('{}: {}'.format(description, ' '.join(command_line)))
        rv = call(command_line)
        if rv != 0:
            exit(rv)

    if fix_imports:
        execute_tool('Fixing import order', 'isort', '-rc')
    execute_tool('Checking code style', 'flake8')


@click.command()
def clean():
    """Remove *.pyc and *.pyo files recursively starting at current directory.

    Borrowed from Flask-Script, converted to use Click.
    """
    for dirpath, dirnames, filenames in os.walk('.'):
        for filename in filenames:
            if filename.endswith('.pyc') or filename.endswith('.pyo'):
                full_pathname = os.path.join(dirpath, filename)
                click.echo('Removing {}'.format(full_pathname))
                os.remove(full_pathname)


@click.command()
@with_appcontext
def init_data():
    from smp_cms.scripts import init_script
    init_script.init_data()

@click.command()
@with_appcontext
def total_delete():
    import os, shutil
    from flask_migrate import migrate, upgrade, init
    from smp_cms.scripts import init_script

    try:
        os.remove(os.path.join(PROJECT_ROOT, 'dev.db'))
    except:
        print("Already deleted dev.db")

    try:
        shutil.rmtree(os.path.join(PROJECT_ROOT, 'migrations'))
    except:
        print("Already deleted migrations")

    init()
    migrate()
    upgrade()
    init_script.init_data()


@click.command()
@with_appcontext
def generate_patient():
    from tests.api_test_patient import generate_patient
    for i in range(1,1000):

        parent_id = None
        set_gender = random.choice(['female', 'male'])
        birthday = None
        is_ln = random.choice([True, False])
        is_name = random.choice([True, False])
        is_sn = random.choice([True, False])
        is_snils = random.choice([True, False])
        is_bday = random.choice([True, False])
        is_death = random.choice([True, False])
        is_polis = random.choice([True, False])
        is_docs = random.choice([True, False])

        is_newborn = random.choice([True, False])
        if is_newborn:
            mother_age = relativedelta(years=random.randint(18,30))
            parent = generate_patient(set_gender='female', birthday=(datetime.now() - mother_age).date())
            parent_id = parent.json()['patient']['id']
            is_ln = False
            is_name = False
            is_snils = False
            is_polis = False
            is_docs = False
            birthday = datetime.now().date()

        pat = generate_patient(parent_id=parent_id, set_gender=set_gender, birthday=birthday,
                 is_ln=is_ln, is_name=is_name, is_sn=is_sn,
                 is_snils=is_snils, is_bday=is_bday, is_death=is_death,
                 is_polis=is_polis, is_docs=is_docs)
        print(pat)


@click.command()
@click.option('--url', default=None,
              help='Url to test (ex. /static/image.png)')
@click.option('--order', default='rule',
              help='Property on Rule to order by (default: rule)')
@with_appcontext
def urls(url, order):
    """Display all of the url matching routes for the project.

    Borrowed from Flask-Script, converted to use Click.
    """
    rows = []
    column_length = 0
    column_headers = ('Rule', 'Endpoint', 'Arguments')

    if url:
        try:
            rule, arguments = (
                current_app.url_map
                           .bind('localhost')
                           .match(url, return_rule=True))
            rows.append((rule.rule, rule.endpoint, arguments))
            column_length = 3
        except (NotFound, MethodNotAllowed) as e:
            rows.append(('<{}>'.format(e), None, None))
            column_length = 1
    else:
        rules = sorted(
            current_app.url_map.iter_rules(),
            key=lambda rule: getattr(rule, order))
        for rule in rules:
            rows.append((rule.rule, rule.endpoint, None))
        column_length = 2

    str_template = ''
    table_width = 0

    if column_length >= 1:
        max_rule_length = max(len(r[0]) for r in rows)
        max_rule_length = max_rule_length if max_rule_length > 4 else 4
        str_template += '{:' + str(max_rule_length) + '}'
        table_width += max_rule_length

    if column_length >= 2:
        max_endpoint_length = max(len(str(r[1])) for r in rows)
        # max_endpoint_length = max(rows, key=len)
        max_endpoint_length = (
            max_endpoint_length if max_endpoint_length > 8 else 8)
        str_template += '  {:' + str(max_endpoint_length) + '}'
        table_width += 2 + max_endpoint_length

    if column_length >= 3:
        max_arguments_length = max(len(str(r[2])) for r in rows)
        max_arguments_length = (
            max_arguments_length if max_arguments_length > 9 else 9)
        str_template += '  {:' + str(max_arguments_length) + '}'
        table_width += 2 + max_arguments_length

    click.echo(str_template.format(*column_headers[:column_length]))
    click.echo('-' * table_width)

    for row in rows:
        click.echo(str_template.format(*row[:column_length]))
