from smp_cms.utils.database import db, ModelGetByIdMixin, Timestamps,  Model



class Role(Model, ModelGetByIdMixin, Timestamps):
    __tablename__ = 'roles'
    id = db.Column(db.Integer(), primary_key=True)
    type = db.Column(db.Integer())
    name = db.Column(db.String(255))
    default_endpoint = db.Column(db.String(255))

    def as_dict(self):
        role={}
        return role


    def __repr__(self):
        return '<Role %r>' % (self.type)


