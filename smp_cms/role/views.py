from flask import Blueprint, jsonify
from smp_cms.constants import *

blueprint = Blueprint('/users', __name__)


@blueprint.route("status", methods=["GET"])
def get_user():
    return jsonify(SOCIAL_STATUSES)

