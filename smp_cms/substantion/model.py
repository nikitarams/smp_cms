from smp_cms.utils.database import db, ModelGetByIdMixin, Timestamps,  Model



class Substantion(Model, ModelGetByIdMixin, Timestamps):
    __tablename__ = 'substantions'
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(255))

    def as_dict(self):
        substantion={}
        return substantion


    def __repr__(self):
        return '<Substantion %r>' % (self.name)


