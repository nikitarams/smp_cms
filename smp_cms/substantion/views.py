import requests
from flask import Blueprint, jsonify, request, current_app, redirect
from flask_login import login_user, logout_user, current_user
from sqlalchemy.exc import IntegrityError
from .model import User
# from ..utils.auth import generate_token, auth_required, verify_token, access_permssion, only_su
# from ..utils.resolutions import Resolution
# from ..utils.database import db
from smp_cms.constants import *

blueprint = Blueprint('/users', __name__)



@blueprint.route("status", methods=["GET"])
def get_user():
    return jsonify(SOCIAL_STATUSES)

