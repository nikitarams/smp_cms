from functools import wraps
from flask import request, jsonify, current_app
from flask_login import current_user, login_user
from smp_cms.user.models import User
from smp_cms.utils.exceptions import InvalidUsage
from smp_cms.utils.resolutions import Resolution


