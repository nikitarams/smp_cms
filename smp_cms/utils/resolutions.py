from flask import jsonify, current_app

class Resolution():

    def __init__(self, payload=None, **kwargs):
        self.payload = payload
        self.kwargs = kwargs

    def user_created(self):
        current_app.logger.info('User with email %s registered', self.kwargs['email'])
        return jsonify(self.payload.json), 302

    def user_updated(self):
        current_app.logger.info('User with email %s profile updated', self.kwargs['email'])
        return jsonify(self.payload.json), 200

    def user_logged_in(self):
        current_app.logger.info('User with email %s logged in', self.kwargs['email'])
        return jsonify(self.payload.json), 302

    def user_already_registered(self):
        current_app.logger.info('Wrong type of OAuth registration was attempted - %s', self.kwargs['type'])
        return jsonify('Wrong type of OAuth registration was attempted'), 400

    def wrong_field(self):
        response = ('Wrong field {0} was detected in request {1}').format(self.kwargs['field'], self.payload.json)
        current_app.logger.info(response)
        return jsonify(response), 400

    def user_oauth_register_failed(self):
        current_app.logger.info('OAuth registration / authenfication with type %s was failed', self.kwargs['type'])
        return jsonify('Wrong type of OAuth registration was attempted'), 401

    def wrong_credentials(self):
        current_app.logger.info('Attempt to login without credentials')
        return jsonify('Attempt to login without credentials'), 400

    def user_not_found(self):
        current_app.logger.info('User with email %s was not registered', self.kwargs['email'])
        return jsonify(('User with email {} was not registered').format(self.kwargs['email']) ), 401

    def wrong_oauth_type(self):
        current_app.logger.info('User with email %s already registered', self.kwargs['email'])
        return jsonify(self.payload), 409

    def user_logged_out(self):
        current_app.logger.info('User with email %s logged out', self.kwargs['email'])
        return jsonify(self.payload), 200

    def user_info_read(self):
        current_app.logger.info('User with email %s information send', self.kwargs['email'])
        return jsonify(self.payload.json), self.payload.status_code

    def access_denied(self):
        response = ('User with id {0} is trying to access restricted api').format(self.payload.json['user_id'])
        current_app.logger.info(response)
        return jsonify(response), 401

    def access_denied_su_only(self):
        response = ('User with email {} is trying to access restricted api for SU only').format(self.kwargs['email'])
        current_app.logger.info(response)
        return jsonify(response), 401

    def food_found(self):
        current_app.logger.info('User %s recieved a food', self.kwargs['email'])
        return jsonify(self.payload), 200

    def no_food_found(self):
        current_app.logger.info('User %s did not find a food %s', self.kwargs['email'], self.kwargs['search_food'])
        return jsonify([]), 200

    def ingredient_found(self):
        current_app.logger.info('User %s recieved an ingredient', self.kwargs['email'])
        return jsonify(self.payload), 200

    def new_meal_created(self):
        current_app.logger.info('User %s created new meal %s', self.kwargs['email'], self.kwargs['new_meal_id'])
        return jsonify(self.payload), 200

    def food_stat(self):
        current_app.logger.info('User %s recieved a statistics', self.kwargs['email'])
        return jsonify(self.payload), 200

    def new_food_created(self):
        current_app.logger.info('User %s created new food %s', self.kwargs['email'], self.kwargs['new_food_id'])
        return jsonify(self.payload), 200

    def payment_created(self):
        current_app.logger.info('User %s - new payment arrived %s', self.kwargs['email'], self.kwargs['payment_id'])
        return jsonify(self.payload), 200

    def user_posted_comment(self):
        current_app.logger.info('User %s - new comment posted in meal %s', self.kwargs['email'], self.kwargs['meal_id'])
        return jsonify(self.payload), 200

    def root_comment_already_exists(self):
        current_app.logger.info('User %s - was trying to post another root comment in meal %s',
                                self.kwargs['email'], self.kwargs['meal_id'])
        return jsonify(self.payload), 409

    def parent_comment_not_found(self):
        current_app.logger.info('User %s - was trying to post an answer to comment in meal %s',
                                self.kwargs['email'], self.kwargs['meal_id'])
        return jsonify(self.payload), 404

    def comment_not_found(self):
        current_app.logger.info('Comment with id = %s was not found in meal %s',
                                self.kwargs['comment_id'], self.kwargs['meal_id'])
        return jsonify(self.payload), 404

    def user_edited_comment(self):
        current_app.logger.info('User %s - comment %s was edited in meal %s', self.kwargs['email'],
                                self.kwargs['comment_id'], self.kwargs['meal_id'])
        return jsonify(self.payload), 200

    def user_deleted_comment(self):
        current_app.logger.info('User %s - comment %s was deleted in meal %s', self.kwargs['email'],
                                self.kwargs['comment_id'], self.kwargs['meal_id'])
        return jsonify(self.payload), 200

    def payment_updated(self):
        current_app.logger.info('User %s - payment %s', self.kwargs['email'],
                                self.kwargs['payment_id'])
        return jsonify(self.payload.json), 200

