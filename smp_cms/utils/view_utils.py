# -*- coding: utf-8 -*-
"""Helper utilities and decorators."""
from functools import wraps
from flask import request, flash, render_template
from smp_cms.user.models import User
from flask_login import current_user
from smp_cms.constants import *
from random import randint


def random_with_N_digits(n):
    range_start = 10**(n-1)
    range_end = (10**n)-1
    return randint(range_start, range_end)

def is_api(func):
    @wraps(func)
    def decorated(**kwargs):
        if '/api/' in request.path:
            kwargs['is_api'] = True
        else:
            kwargs['is_api'] = False
        return func(**kwargs)
    return decorated


def convert_input_to(class_):
    def wrap(f):
        def decorator(*args):
            obj = class_(**request.get_json())
            return f(obj)
        return decorator
    return wrap

def flash_errors(form, category='warning'):
    """Flash all errors for a form."""
    for field, errors in form.errors.items():
        for error in errors:
            flash('{0} - {1}'.format(getattr(form, field).label.text, error), category)


def get_user_by_id(func):
    @wraps(func)
    def decorated(**kwargs):
        user = User.get_by_id(kwargs['id'])
        if current_user and current_user.is_authenticated:
            if (current_user.role.id in PERMISSION_LOOK_USER or current_user == user):
                if user:
                    del kwargs['id']
                    kwargs['user'] = user
                    return func(**kwargs)
                else:
                    return render_template('404.html'), 404
            else:
                return render_template('401.html'), 401
        else:
            return render_template('401.html'), 401
    return decorated