# -*- coding: utf-8 -*-
"""Helper utilities and decorators."""
from flask import flash
import decimal, datetime, json, random
from sqlalchemy import inspect
from .database import db

def get_or_create(model, **kwargs):
    instance = model.query.filter_by(**kwargs).first()
    if instance:
        return instance
    else:
        instance = model(**kwargs)
        instance.save()
        return instance

def get_or_update(model, entity, **kwargs):
    instance = model.query.filter_by(**kwargs).first()
    if instance:
        return instance
    else:
        kwargs['birthday'] = datetime.datetime.strptime(kwargs['birthday'], '%Y-%m-%d')
        entity.update(**kwargs)
        return entity

def luhn_residue(digits):
    return sum(sum(divmod(int(d)*(1 + i%2), 10))
                 for i, d in enumerate(digits[::-1])) % 10

def generate_oms():
    part = ''.join(str(random.randrange(0, 9)) for _ in range(16 - 1))
    res = luhn_residue('{}{}'.format(part, 0))
    return '{}{}'.format(part, -res % 10)

def generate_snils():
    nums = [
        random.randint(1, 1) if x == 0
        else '-' if x == 3
        else '-' if x == 7
        else ' ' if x == 11
        else random.randint(0, 9)
        for x in range(0, 12)
    ]

    cont = (nums[10] * 1) + (nums[9] * 2) + (nums[8] * 3) + \
           (nums[6] * 4) + (nums[5] * 5) + (nums[4] * 6) + \
           (nums[2] * 7) + (nums[1] * 8) + (nums[0] * 9)

    if cont in (100, 101):
        cont = '00'

    elif cont > 101:
        cont = (cont % 101)
        if cont in (100, 101): cont = '00'
        elif cont < 10: cont = '0' + str(cont)

    elif cont < 10: cont = '0' + str(cont)

    nums.append(cont)
    return ''.join([str(x) for x in nums])

def check_snils(snils):
    snils = snils.replace('-', '').replace(' ', '')
    if len(snils) != 11:
        return False

    def snils_csum(snils):
        k = range(9, 0, -1)
        pairs = zip(k, [int(x) for x in snils[:-2]])
        return sum([k * v for k, v in pairs])

    csum = snils_csum(snils)

    while csum > 101:
        csum %= 101
    if csum in (100, 101):
        csum = 0

    return csum == int(snils[-2:])

def alchemyencoder(obj):
    """JSON encoder function for SQLAlchemy special classes."""
    if isinstance(obj, datetime.date):
        return obj.isoformat()
    elif isinstance(obj, decimal.Decimal):
        return float(obj)

def flash_errors(form, category='warning'):
    """Flash all errors for a form."""
    for field, errors in form.errors.items():
        for error in errors:
            flash('{0} - {1}'.format(getattr(form, field).label.text, error), category)


class JsonModel(object):
    def as_dict_glob(self):
        d = {}
        for column in self.__table__.columns:
            if column.name not in ['created_at', 'updated_at']:
                d[column.name] = str(getattr(self, column.name))
        return d

    def object_as_dict(self):
        return {c.key: getattr(self, c.key)
                for c in inspect(self).mapper.column_attrs}

    def object_as_dict_with_keys(self, keys):
        return {c.key: getattr(self, c.key)
                for c in inspect(self).mapper.column_attrs if c.key in keys}

    def to_json(self):
        return json.dumps(self.object_as_dict(), default=alchemyencoder)

    def to_json_pretty(self):
        return "<br />".join(json.dumps(self.object_as_dict(),indent=True,sort_keys=True,default=alchemyencoder).split("\n"))

    def from_json(self, fields, j):
        [self.__setattr__(f, j.get(f, None)) for f in fields]
        return self