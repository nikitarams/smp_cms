from functools import wraps
from smp_cms.patient.model import Patient

def get_patient(func):
    @wraps(func)
    def decorated(**kwargs):
        patient = Patient.get_by_id_or_404(kwargs['patient_id'])
        return func(patient)
    return decorated
