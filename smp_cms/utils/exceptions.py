from flask import jsonify, current_app


def template(data, code=500):
    return {'message': {'errors': {'body': data}}, 'status_code': code}


USER_NOT_FOUND = template(['User not found'], code=404)
AUTH_USER_NOT_FOUND = template(['auth user not found'], code=404)
USER_ALREADY_REGISTERED = template(['User already registered'], code=422)
UNKNOWN_ERROR = template([], code=500)
SIMPLE_ERROR = template(['Simple Error'], code=500)
NO_FOOD_FOUND = template(['No food found'], code=404)
NO_FOOD_CREATED = template(['No food created'], code=422)
NO_MEAL_CREATED = template(['No meal created'], code=422)
NO_SEARCH_ARGS_FOUND = template(['Insufficient search arguments'], code=404)
ACCESS_DENIED = template(['Access is denied'], code=401)
USER_OAUTH_REGISTER_FAIL = template(['User oauth register failed'], code=500)
ROOT_COMMENT_EXISTS = template(['Root comment already exists'], code=422)
PARENT_COMMENT_DO_NOT_EXISTS = template(['Parent comment do not exists'], code=404)
COMMENT_DO_NOT_EXISTS = template(['Comment do not exists'], code=404)


class InvalidUsage(Exception):
    status_code = 500

    def __init__(self, message, status_code=None, payload=None):
        Exception.__init__(self)
        self.message = message
        if status_code is not None:
            self.status_code = status_code
        self.payload = payload

    def to_json(self):
        rv = self.message
        return jsonify(rv)

    @classmethod
    def user_not_found(cls):
        current_app.logger.error('User not found')
        return cls(**USER_NOT_FOUND)

    @classmethod
    def auth_user_not_found(cls):
        current_app.logger.error('Auth User not found')
        return cls(**AUTH_USER_NOT_FOUND)

    @classmethod
    def access_denied(cls):
        current_app.logger.error('Access is denied')
        return cls(**ACCESS_DENIED)

    @classmethod
    def simple_error(self):
        code = 500
        error_text = "Simple Error"
        current_app.logger.error(error_text)
        return jsonify(error_text), code

    @classmethod
    def user_already_registered(cls):
        current_app.logger.error('User already registered')
        return cls(**USER_ALREADY_REGISTERED)

    @classmethod
    def user_oauth_register_failed(cls):
        current_app.logger.error('User oauth register failed')
        return cls(**USER_OAUTH_REGISTER_FAIL)

    @classmethod
    def no_food_found(cls):
        current_app.logger.warning('No food found')
        return cls(**NO_FOOD_FOUND)

    @classmethod
    def no_food_created(cls, message):
        current_app.logger.warning('No food created, error message: ' + str(message))
        return cls(**NO_FOOD_CREATED)

    @classmethod
    def no_meal_created(cls, message):
        current_app.logger.warning('No meal created, error message: ' + str(message))
        return cls(**NO_MEAL_CREATED)

    @classmethod
    def no_search_args(cls):
        current_app.logger.error('Insufficient search arguments')
        return cls(**NO_SEARCH_ARGS_FOUND)

    @classmethod
    def root_comment_already_exists(cls):
        current_app.logger.error('Root comment already exists')
        return cls(**ROOT_COMMENT_EXISTS)

    @classmethod
    def parent_comment_not_found(cls):
        current_app.logger.error('Parent comment do not exists')
        return cls(**PARENT_COMMENT_DO_NOT_EXISTS)

    @classmethod
    def comment_not_found(cls):
        current_app.logger.error('Comment do not exists')
        return cls(**COMMENT_DO_NOT_EXISTS)

    @classmethod
    def unknown_error(cls):
        current_app.logger.error('Unknown error')
        return cls(**UNKNOWN_ERROR)
