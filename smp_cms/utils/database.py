# -*- coding: utf-8 -*-
from sqlalchemy.orm import relationship
from flask_bcrypt import Bcrypt
from flask_migrate import Migrate
from flask_caching import Cache
from .compat import basestring
from smp_cms.extensions import db

bcrypt = Bcrypt()
migrate = Migrate()
cache = Cache()
Column = db.Column
relationship = relationship
Model = db.Model


class Timestamps(object):
    """
    Useful mixin to add generic timestamps.
    Recommended to use on most of the models.
    """
    created_at = db.Column(db.DateTime, default=db.func.now())
    updated_at = db.Column(db.DateTime, default=db.func.now(), onupdate=db.func.now())

class ModelGetByIdMixin(object):
    @classmethod
    def get_by_id(cls, id):
        if any(
                (isinstance(id, basestring) and id.isdigit(),
                 isinstance(id, (int, float))),
        ):
            return cls.query.get(int(id))
        return None

    @classmethod
    def get_by_id_or_404(cls, id, project=None):
        if any(
                (isinstance(id, basestring) and id.isdigit(),
                 isinstance(id, (int, float))),
        ):
            res = cls.query.get(int(id))
            if res is not None:
                return res
            return None

def ReferenceCol(tablename, nullable=False, pk_name='id', **kwargs):
    """Column that adds primary key foreign key reference.

    Usage: ::

        category_id = ReferenceCol('category')
        category = relationship('Category', backref='categories')
    """
    return db.Column(
        db.ForeignKey("{0}.{1}".format(tablename, pk_name)),
        nullable=nullable, **kwargs)


class SurrogatePK(object):
    """A mixin that adds a surrogate integer 'primary key' column named ``id`` \
        to any declarative-mapped class.
    """

    __table_args__ = {'extend_existing': True}

    id = db.Column(db.Integer, primary_key=True)

    @classmethod
    def get_by_id(cls, record_id):
        """Get record by ID."""
        if any(
                (isinstance(record_id, basestring) and record_id.isdigit(),
                 isinstance(record_id, (int, float))),
        ):
            return cls.query.get(int(record_id))


def reference_col(tablename, nullable=False, pk_name='id', **kwargs):
    """Column that adds primary key foreign key reference.

    Usage: ::

        category_id = reference_col('category')
        category = relationship('Category', backref='categories')
    """
    return db.Column(
        db.ForeignKey('{0}.{1}'.format(tablename, pk_name)),
        nullable=nullable, **kwargs)


class CRUDMixin(object):
    """Mixin that adds convenience methods for CRUD (create, read, update, delete) operations."""

    @classmethod
    def create(cls, **kwargs):
        """Create a new record and save it the database."""
        instance = cls(**kwargs)
        return instance.save()

    def update(self, commit=True, **kwargs):
        """Update specific fields of a record."""
        for attr, value in kwargs.items():
            setattr(self, attr, value)
        return commit and self.save() or self

    def save(self, commit=True):
        """Save the record."""
        db.session.add(self)
        if commit:
            db.session.commit()
        return self

    def delete(self, commit=True):
        """Remove the record from the database."""
        db.session.delete(self)
        return commit and db.session.commit()


class Model(CRUDMixin, db.Model):
    __abstract__ = True
    all_model_watch_registry = set()

    def __init_subclass__(cls, **kwargs):
        super().__init_subclass__(**kwargs)

    @classmethod
    def check_fields(cls, income_dict):
        diff = set(income_dict.keys()) - set(cls.get_model_dict().keys())
        if len(diff):
            wrong_field = ', '.join(str(e) for e in diff)
            return {"error": "Wrong field detected", "field": wrong_field}
        for key, value in income_dict.items():
            setattr(cls, key, value)
        return cls

    @classmethod
    def get_model_dict(cls):
        return dict((column.name, getattr(cls, column.name))
                    for column in cls.__table__.columns)
