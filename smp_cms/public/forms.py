# -*- coding: utf-8 -*-
"""Public forms."""
from flask_wtf import FlaskForm
from wtforms import PasswordField, StringField
from wtforms.validators import DataRequired

from smp_cms.user.models import User


class LoginForm(FlaskForm):
    """Login form."""

    tab_id = StringField('Табельный номер', validators=[DataRequired()])
    password = PasswordField('Пароль', validators=[])

    def __init__(self, *args, **kwargs):
        """Create instance."""
        super(LoginForm, self).__init__(*args, **kwargs)
        self.user = None

    def validate(self):
        """Validate the form."""
        initial_validation = super(LoginForm, self).validate()
        if not initial_validation:
            return False

        self.user = User.query.filter_by(tab_id=self.tab_id.data).first()
        if not self.user:
            self.tab_id.errors.append('Неверный табельный номер')
            return False

        if not self.user.check_password(self.password.data):
            self.password.errors.append('Неверный пароль')
            return False

        if not self.user.active:
            self.tab_id.errors.append('Пользователь заблокирован')
            return False
        return True
