# -*- coding: utf-8 -*-
"""Public section, including homepage and signup."""
from flask import Blueprint, flash, redirect, render_template, request, url_for
from flask_login import login_required, login_user, logout_user

from smp_cms.extensions import login_manager
from smp_cms.public.forms import LoginForm
from smp_cms.user.models import User
from smp_cms.utils.view_utils import flash_errors

blueprint = Blueprint('public', __name__, static_folder='../static')


@login_manager.user_loader
def load_user(user_id):
    """Load user by ID."""
    return User.get_by_id(int(user_id))


@blueprint.route('/', methods=['GET', 'POST'])
def home():
    """Home page."""
    form = LoginForm(request.form)
    # Handle logging in
    if request.method == 'POST':
        if form.validate_on_submit():
            login_user(form.user)
            flash('You are logged in.', 'success')
            redirect_url = request.args.get('next') or url_for(form.user.role.default_endpoint)
            return redirect(redirect_url)
        else:
            flash_errors(form)
    return render_template('public/home.html', login_form = form)


@blueprint.route('/logout/')
@login_required
def logout():
    """Logout."""
    logout_user()
    flash('You are logged out.', 'info')
    return redirect(url_for('public.home'))


@blueprint.route('/about/')
def about():
    """About page."""
    form = LoginForm(request.form)
    return render_template('public/about.html')


@blueprint.route('/users')
def users():
    users_db = User.query.all()
    users = []
    for u in users_db:
        users.append([u.id, u.tab_id, u.role.name, u.fio(long=True)])
    return render_template('users/users.html',
                           headers=['id', 'Таб номер', 'Роль', 'ФИО'],
                           items=users,
                           title='сводка по пользователям')
