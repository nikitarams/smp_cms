from smp_cms.user.models import User
from smp_cms.role.model import Role
from smp_cms.patient.model import DocumentType, PolicyType, Smo
from smp_cms.call.model import *
from smp_cms.services.nsi.models import Settings
from datetime import datetime
from flask import current_app
from smp_cms.utils.database import db
from smp_cms.constants import *
import xml.etree.ElementTree as ET
import os, io
from dbfread import DBF as dbf

def generate_roles():
    db.session.query(User).delete()
    db.session.query(Role).delete()
    for role in ROLES:
        user_role = Role(name=role['title'], type=role['role'], default_endpoint=role['url'])
        user_role.save()
        User._bootstrap(user_role, role['number'])


def generate_doc_types():
    db.session.query(DocumentType).delete()
    for doc_type in DOC_TYPES:
        DocumentType._bootstrap(doc_type)


def generate_policy_types():
    db.session.query(PolicyType).delete()
    # for pol_type in POLICY_TYPES:
    #     PolicyType._bootstrap(pol_type)

    file_name = os.path.abspath(os.path.join(current_app.config['OMS_NSI'], PolicyType().nsi_file))
    root = ET.parse(file_name).getroot()
    items = []
    for type_tag in root.findall('zap'):
        items.append(PolicyType(id=type_tag.find('IDDOC').text,
                                title=type_tag.find('DOCNAME').text))
    db.session.bulk_save_objects(items)
    db.session.commit()


def generate_mkb10():
    db.session.query(Mkb10).delete()
    items = []
    file_name = os.path.abspath(os.path.join(os.path.dirname(__file__), Mkb10().nsi_file))
    with io.open(file_name, encoding='utf-8') as file:
        for line in file:
            row = line.replace('\n', '').split("\t")
            items.append(Mkb10(code=row[1], title=row[2].strip(), is_emergency=True if row[3] else False,
                               is_oms=True if row[4] else False))
    db.session.bulk_save_objects(items)
    db.session.commit()


def generate_place():
    db.session.query(Place).delete()
    items = []
    file_name = os.path.abspath(os.path.join(os.path.dirname(__file__), Place().nsi_file))
    with io.open(file_name, encoding='utf-8-sig') as file:
        for line in file:
            row = line.replace('\n', '').split(";")
            items.append(Place(id=int(row[0]), title=row[1]))
    db.session.bulk_save_objects(items)
    db.session.commit()


def generate_who_call():
    db.session.query(WhoCall).delete()
    items = []
    file_name = os.path.abspath(os.path.join(os.path.dirname(__file__), WhoCall().nsi_file))
    with io.open(file_name, encoding='utf-8-sig') as file:
        for line in file:
            row = line.replace('\n', '').split(";")
            items.append(WhoCall(id=row[0], title=row[1]))
    db.session.bulk_save_objects(items)
    db.session.commit()


def generate_profile():
    db.session.query(Profile).delete()
    items = []
    file_name = os.path.abspath(os.path.join(os.path.dirname(__file__), Profile().nsi_file))
    with io.open(file_name, encoding='utf-8-sig') as file:
        for line in file:
            row = line.replace('\n', '').split(";")
            items.append(Profile(id=row[0], name=row[1], title=row[2]))
    db.session.bulk_save_objects(items)
    db.session.commit()


def generate_reason():
    db.session.query(Reason).delete()
    items = []
    file_name = os.path.abspath(os.path.join(os.path.dirname(__file__), Reason().nsi_file))
    with io.open(file_name, encoding='utf-8-sig') as file:
        for line in file:
            row = line.replace('\n', '').split(";")
            items.append(Reason(id=row[0], title=row[1]))
    db.session.bulk_save_objects(items)
    db.session.commit()


def generate_trauma():
    db.session.query(TraumaType).delete()
    items = []
    file_name = os.path.abspath(os.path.join(current_app.config['OMS_NSI'], TraumaType().nsi_file))

    dbf_file = dbf(file_name, load=True)
    for rec in dbf_file:
        items.append(TraumaType(id=rec["ID_TRAVM"], title=rec["NAME"]))
    db.session.bulk_save_objects(items)
    db.session.commit()

def generate_tarif():
    db.session.query(Tarif).delete()
    items = []
    file_name = os.path.abspath(os.path.join(current_app.config['OMS_NSI'], Tarif().nsi_file))

    dbf_file = dbf(file_name, load=True)
    for rec in dbf_file:
        items.append(Tarif(code=rec["US_KOD"], title=rec["NAME"],
                           tarif=rec["TARIF"], period_start=rec["DATEBEG"],
                           period_end=rec["DATEEND"]))
    db.session.bulk_save_objects(items)
    db.session.commit()

def generate_trauma():
    db.session.query(ProfessionType).delete()
    file_name = os.path.abspath(os.path.join(current_app.config['OMS_NSI'], ProfessionType().nsi_file))
    root = ET.parse(file_name).getroot()
    items = []
    for type_tag in root.findall('zap'):
        items.append(ProfessionType(code=type_tag.find('IDSPEC').text,
                         title=type_tag.find('SPECNAME').text))

    db.session.bulk_save_objects(items)
    db.session.commit()


def generate_smo():
    db.session.query(Smo).delete()
    file_name = os.path.abspath(os.path.join(current_app.config['OMS_NSI'], Smo().nsi_file))
    root = ET.parse(file_name).getroot()
    items = []
    for type_tag in root.findall('insCompany'):
        items.append(Smo(id=type_tag.find('smocod').text,
                         okato=type_tag.find('tf_okato').text,
                         ogrn=type_tag.find('Ogrn').text,
                         inn=type_tag.find('inn').text,
                         kpp=type_tag.find('KPP').text,
                         full_title=type_tag.find('nam_smop').text,
                         short_title=type_tag.find('nam_smok').text))

    db.session.bulk_save_objects(items)
    db.session.commit()


def generate_settings():
    db.session.query(Settings).delete()
    mo_title = 'ГБУЗ ВО "СCМП Г. ВЯЗНИКИ"'
    mo_title_full = 'ГОСУДАРСТВЕННОЕ БЮДЖЕТНОЕ УЧРЕЖДЕНИЕ ЗДРАВООХРАНЕНИЯ ВЛАДИМИРСКОЙ ОБЛАСТИ "СТАНЦИЯ СКОРОЙ МЕДИЦИНСКОЙ ПОМОЩИ Г. ВЯЗНИКИ"'
    mo_inn = 3303005229
    mo_kpp = 330301001
    mo_director = 'УСТИНОВА	ТАТЬЯНА	СТЕПАНОВНА'
    mo_glavbuh = 'УСТИНОВА	ТАТЬЯНА	СТЕПАНОВНА'
    reestr_version = '3.1'
    mo_reestr_code = '330333'

    settings = Settings(
        mo_title=mo_title,
        mo_title_full=mo_title_full,
        mo_inn=mo_inn,
        mo_kpp=mo_kpp,
        mo_director=mo_director,
        mo_glavbuh=mo_glavbuh,
        reestr_version=reestr_version,
        mo_reestr_code=mo_reestr_code
    )
    settings.save()
    db.session.commit()

def generate_results():
    db.session.query(Result).delete()
    file_name = os.path.abspath(os.path.join(current_app.config['OMS_NSI'], str(Result().nsi_file)))
    root = ET.parse(file_name).getroot()
    items = []
    for type_tag in root.findall('zap'):
        code = int(type_tag.find('IDRMP').text)
        if code >= 400:
            items.append(Result(id=code, title=type_tag.find('RMPNAME').text))
    db.session.bulk_save_objects(items)
    db.session.commit()


def init_data():
    with current_app.app_context():
        generate_results()
        generate_roles()
        generate_doc_types()
        generate_policy_types()
        generate_smo()
        generate_mkb10()
        generate_settings()
        generate_trauma()
        generate_trauma()
        generate_tarif()
        generate_place()
        generate_who_call()
        generate_reason()
        generate_profile()