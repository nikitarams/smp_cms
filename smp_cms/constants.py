def GENERATE_CHOICES(lookup):
    choices = list()
    for key in lookup:
        choices.append((key, lookup[key]))
    return choices


def GENERATE_OPTIONS(lookup):
    options = list()
    for key in lookup:
        options.append(key)
    return options


SOCIAL_STATUSES = [{'S': 'неработающий', 'F': 'работающий', 'Е': 'бомж', 'L': 'студент дневной формы обучения'}]

ROLE_ADMIN = 0
ROLE_GLAV_VRACH = 1
ROLE_STAR_VRACH = 2
ROLE_DISP_03 = 3
ROLE_DISP_NAPR = 4
ROLE_DISP_PS = 5
ROLE_FELDSHER = 6
ROLE_DOCTOR = 7

ROLE_ADMIN_STR = 'Администартор'
ROLE_GLAV_VRACH_STR = 'Главный врач'
ROLE_STAR_VRACH_STR = 'Старший врач'
ROLE_DISP_03_STR = 'Диспетчер 03'
ROLE_DISP_NAPR_STR = 'Диспетчер направления'
ROLE_DISP_PS_STR = 'Диспетчер подстанции'
ROLE_FELDSHER_STR = 'Фельдшер выездной бригады'
ROLE_DOCTOR_STR = 'Врач выездной бригады'

ROLES = [
    {'role': ROLE_ADMIN, 'title': ROLE_ADMIN_STR, 'number': 1, 'url': 'services.dashboard_common'},
    {'role': ROLE_GLAV_VRACH, 'title': ROLE_GLAV_VRACH_STR, 'number': 1, 'url': 'services.dashboard_common'},
    {'role': ROLE_STAR_VRACH, 'title': ROLE_STAR_VRACH_STR, 'number': 4, 'url': 'services.oo_common'},
    {'role': ROLE_DISP_03, 'title': ROLE_DISP_03_STR, 'number': 10, 'url': 'services.oo_common'},
    {'role': ROLE_DISP_NAPR, 'title': ROLE_DISP_NAPR_STR, 'number': 10, 'url': 'services.oo_common'},
    {'role': ROLE_DISP_PS, 'title': ROLE_DISP_PS_STR, 'number': 5, 'url': 'services.oo_common'},
    {'role': ROLE_FELDSHER, 'title': ROLE_FELDSHER_STR, 'number': 100, 'url': 'services.lk_common'},
    {'role': ROLE_DOCTOR, 'title': ROLE_DOCTOR_STR, 'number': 20, 'url': 'services.lk_common'},
]

PERMISSION_LOOK_USER = [ROLE_ADMIN, ROLE_GLAV_VRACH_STR, ROLE_STAR_VRACH_STR]

SUBSTATIONS = {
    1: 'Первая',
    2: 'Вторая',
    3: 'Третья',
    4: 'Четвертая',
    5: 'Пятая',
}

MENU_OO = 0
MENU_DASHBOARD = 1
MENU_NSI = 3
MENU_ARCHIVE = 4
MENU_LK = 5
MENU_USERS = 6

MENU_OO_STR = 'Оперативная обстановка'
MENU_DASHBOARD_STR = 'Панель управления'
MENU_NSI_STR = 'Нормативно-справочная инфа'
MENU_ARCHIVE_STR = 'Архив'
MENU_LK_STR = 'Личный кабинет'
MENU_USERS_STR = 'Пользователи'

MENUS = [
    {'menu': MENU_OO, 'title': MENU_OO_STR, 'url': 'services.oo_common',
     'roles': [ROLE_ADMIN, ROLE_DISP_NAPR, ROLE_STAR_VRACH, ROLE_FELDSHER, ROLE_DOCTOR]},
    {
        'menu': MENU_DASHBOARD,
        'title': MENU_DASHBOARD_STR,
        'url': 'services.dashboard_common',
        'roles': [ROLE_ADMIN, ROLE_GLAV_VRACH, ROLE_STAR_VRACH]
    },
    {
        'menu': MENU_NSI,
        'title': MENU_NSI_STR,
        'url': 'services.nsi_common',
        'roles': [ROLE_ADMIN, ROLE_STAR_VRACH]
    },
    {
        'menu': MENU_ARCHIVE,
        'title': MENU_ARCHIVE_STR,
        'url': 'services.archive_common',
        'roles': [ROLE_ADMIN, ROLE_STAR_VRACH, ROLE_GLAV_VRACH, ROLE_FELDSHER, ROLE_DOCTOR]
    },
    {
        'menu': MENU_LK,
        'title': MENU_LK_STR,
        'url': 'services.lk_common',
        'roles': [ROLE_ADMIN, ROLE_STAR_VRACH, ROLE_GLAV_VRACH, ROLE_FELDSHER, ROLE_DOCTOR]
    },
    {
        'menu': MENU_USERS,
        'title': MENU_USERS_STR,
        'url': 'public.users',
        'roles': [ROLE_ADMIN, ROLE_STAR_VRACH, ROLE_GLAV_VRACH, ROLE_FELDSHER, ROLE_DOCTOR]
    },
]

DOC_TYPES = [
    [14, "Паспорт гражданина Российской Федерации", "99 99", "9999990"],
    [3, "Свидетельство о рождении", "R-ББ", "999999"],
    [15, "Заграничный паспорт гражданина Российской Федерации", "99", "9999999"],
    [15, "Паспорт иностранного гражданина", "S", "000000000009"],
]

POLICY_TYPES = [
    [3, "Полис ОМС единого образца"],
    [1, "Полис ОМС старого образца"],
    [2, "Временное свидетельство, подтверждающее оформление полиса обязательного медицинского страхования"],
    [4, "Состояние на учёте без полиса ОМС"],
    [5, "Состояние на учёте без временного свидетельства при приёме заявления в иную организацию"],
]

SMO = [
    [33004, 'АО "МАКС-М"'],
    [33007, 'ООО "СК"ИНГОССТРАХ-М"'],
    [33009, 'ООО "КАПИТАЛ МС"'],
]


RUS_LETTERS = "АБВГДЕЖЗИКЛМНОПРСТУФХЦЧШЭЮЯ"