from flask import Blueprint, jsonify, request, current_app, redirect
from sqlalchemy.exc import IntegrityError
from .model import Call, Mkb10
from smp_cms.utils.view_utils import is_api
from tests.api_test_call import generate_call
from smp_cms.utils.model_utils import get_or_create, get_or_update

blueprint = Blueprint('/calls', __name__)


@blueprint.route('/', methods=["GET"])
def get_calls():
    calls = Call.query.all()
    mkbs = Mkb10.query.all()
    return jsonify(calls=calls.as_dict(), mkbs=mkbs.as_dict())


@blueprint.route('/call', methods=["POST"])
@is_api
def create_call(is_api):
    if is_api:
        call = request.get_json()
        pass


@blueprint.route('/api/calls', methods=["POST"])
def api_create_call():
    incoming = request.get_json()

    if ('call' in incoming):
        call = get_or_create(Call, **incoming['call'])


    return jsonify(call=call.as_dict())



######## TESTS
@blueprint.route('/api/fake/calls', methods=["GET"])
def generate_patients():
    res = generate_call()
    return jsonify(res.json())
