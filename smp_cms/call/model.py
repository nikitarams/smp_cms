from smp_cms.utils.database import bcrypt, db, ModelGetByIdMixin, SurrogatePK, Timestamps, ReferenceCol, Model
from sqlalchemy.orm import relationship, backref
from sqlalchemy import UniqueConstraint
import datetime
from calendar import monthrange
from smp_cms.constants import *
from smp_cms.utils.model_utils import JsonModel
from smp_cms.patient.model import Patient
from smp_cms.services.nsi.models import Settings


class ProfessionType(Model, ModelGetByIdMixin, SurrogatePK, JsonModel):
    __tablename__ = 'profession_types'
    nsi_file = "V021.xml"

    code = db.Column(db.String(), index=True)
    title = db.Column(db.String())

    def __init__(self, **kwargs):
        super(ProfessionType, self).__init__(**kwargs)

class Mkb10(Model, ModelGetByIdMixin, SurrogatePK, JsonModel):
    __tablename__ = 'mkb10ds'
    nsi_file = "mkb10ds.csv"

    code = db.Column(db.String(), index=True)
    #TODO - check if code = db.Column(db.String(), primary_key=True) works on postgresql
    title = db.Column(db.String())
    is_emergency = db.Column(db.Boolean())
    is_oms = db.Column(db.Boolean(), default=True)

    def __init__(self, **kwargs):
        super(Mkb10, self).__init__(**kwargs)


class TraumaType(Model, ModelGetByIdMixin, JsonModel):
    __tablename__ = 'trauma_types'
    nsi_file = 'SPR_TRAVM.dbf'

    id = db.Column(db.Integer(), primary_key=True)
    title = db.Column(db.String())

    def __init__(self, **kwargs):
        super(TraumaType, self).__init__(**kwargs)



class CallAction(Model, ModelGetByIdMixin, SurrogatePK, JsonModel):
    __tablename__ = 'call_actions'

    call_id = ReferenceCol('calls', index=True, nullable=False)
    call = relationship('Call', backref=backref("call_actions"))

    t_get = db.Column(db.DateTime)
    t_send = db.Column(db.DateTime)
    t_out = db.Column(db.DateTime)
    t_arrive = db.Column(db.DateTime)
    # TODO - VDS - будет ли отмечаться время выезда, начала, окончания госпитализации
    t_from_home = db.Column(db.DateTime)
    t_to_hosp = db.Column(db.DateTime)
    t_done = db.Column(db.DateTime)
    t_close = db.Column(db.DateTime)
    t_arch = db.Column(db.DateTime)
    t_call_uvd = db.Column(db.DateTime)
    t_delay = db.Column(db.DateTime)

    get_disp_id = ReferenceCol('users', nullable=True)
    get_disp = relationship('User', remote_side="User.id", foreign_keys=[get_disp_id])

    send_disp_id = ReferenceCol('users', nullable=True)
    send_disp = relationship('User', remote_side="User.id", foreign_keys=[send_disp_id])

    out_disp_id = ReferenceCol('users', nullable=True)
    out_disp = relationship('User', remote_side="User.id", foreign_keys=[out_disp_id])

    arrive_disp_id = ReferenceCol('users', nullable=True)
    arrive_disp = relationship('User', remote_side="User.id", foreign_keys=[arrive_disp_id])

    from_home_disp_id = ReferenceCol('users', nullable=True)
    from_home_disp = relationship('User', remote_side="User.id", foreign_keys=[from_home_disp_id])

    to_hosp_disp_id = ReferenceCol('users', nullable=True)
    to_hosp_disp = relationship('User', remote_side="User.id", foreign_keys=[to_hosp_disp_id])

    done_disp_id = ReferenceCol('users', nullable=True)
    done_disp = relationship('User', remote_side="User.id", foreign_keys=[done_disp_id])

    close_disp_id = ReferenceCol('users', nullable=True)
    close_disp = relationship('User', remote_side="User.id", foreign_keys=[close_disp_id])

    call_uvd_disp_id = ReferenceCol('users', nullable=True)
    call_uvd_disp = relationship('User', remote_side="User.id", foreign_keys=[call_uvd_disp_id])

    delay_disp_id = ReferenceCol('users', nullable=True)
    delay_disp = relationship('User', remote_side="User.id", foreign_keys=[delay_disp_id])

    # TODO - спросить какие времена требуются для статистики
    t_wait = db.Column(db.Integer())
    t_go = db.Column(db.Integer())
    t_serving = db.Column(db.Integer())

#
# class DelayTypes(Model, ModelGetByIdMixin, SurrogatePK, JsonModel):
#     __tablename__ = 'delay_types'
#
#     short_title = db.Column(db.String(30))
#     title = db.Column(db.String(100))
#
#
# class Delay(Model, ModelGetByIdMixin, SurrogatePK, Timestamps, JsonModel):
#     __tablename__ = 'delays'
#
#     call_id = ReferenceCol('calls', index=True, nullable=False)
#     call = relationship('Call',
#                         backref=backref("calls", cascade="all,delete", order_by="asc(Call.created_at)"))
#     delay_type_id = ReferenceCol('delay_types', index=True, nullable=False)
#     delay_type = relationship('DelayTypes',
#                             backref=backref("delay_types", cascade="all,delete", order_by="asc(User.created_at)"))
#     comment = db.Column(db.String(200))

class Tarif(Model, ModelGetByIdMixin, SurrogatePK, JsonModel):
    __tablename__ = 'tarifs'
    nsi_file = 'SPR_TAR_2019.DBF'

    code = db.Column(db.Integer())
    title = db.Column(db.String())
    tarif = db.Column(db.Numeric())
    tarif_profile = db.Column(db.String(30))
    period_start = db.Column(db.DateTime)
    period_end = db.Column(db.DateTime)


class Result(Model, ModelGetByIdMixin, JsonModel):
    __tablename__ = 'results'
    nsi_file = 'V009.xml'

    id = db.Column(db.Integer(), primary_key=True)
    title = db.Column(db.String(100))

class Reason(Model, ModelGetByIdMixin, JsonModel):
    __tablename__ = 'reasons'
    nsi_file = 'public_reasn.csv'

    id = db.Column(db.String(), primary_key=True)
    title = db.Column(db.String(100))

class Profile(Model, ModelGetByIdMixin, JsonModel):
    __tablename__ = 'profiles'
    nsi_file = 'public_prof.csv'

    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(1))
    title = db.Column(db.String())


class Place(Model, ModelGetByIdMixin, JsonModel):
    __tablename__ = 'places'
    nsi_file = 'public_place.csv'

    id = db.Column(db.Integer(), primary_key=True)
    title = db.Column(db.String())

class WhoCall(Model, ModelGetByIdMixin, JsonModel):
    __tablename__ = 'who_calls'
    nsi_file = 'public_who_call.csv'

    id = db.Column(db.String(), primary_key=True)
    title = db.Column(db.String())


class OmsStatus(Model, ModelGetByIdMixin, JsonModel):
    __tablename__ = 'oms_statuses'

    id = db.Column(db.Integer(), primary_key=True)
    title = db.Column(db.String(100))


class Turnout(Model, ModelGetByIdMixin, JsonModel):
    __tablename__ = 'turnouts'

    id = db.Column(db.Integer(), primary_key=True)
    call_id = ReferenceCol('calls', index=True, nullable=False)
    call = relationship('Call', backref=backref("turnouts"), remote_side="Call.id", foreign_keys=[call_id])

class Call(Model, ModelGetByIdMixin, SurrogatePK, Timestamps, JsonModel):
    __tablename__ = 'calls'

    year_id = db.Column(db.Integer())
    day_id = db.Column(db.Integer())
    profile_id = ReferenceCol('profiles', index=True, nullable=True)
    profile = relationship('Profile', remote_side="Profile.id", foreign_keys=[profile_id])
    trauma_type_id = ReferenceCol('trauma_types', index=True, nullable=True)
    trauma_type = relationship('TraumaType', remote_side="TraumaType.id", foreign_keys=[trauma_type_id])
    # TODO - передавать протокол опроса в виде текста
    # TODO - нужен ли признак ТЛТ
    protocol = db.Column(db.String(200))
    comment = db.Column(db.String(200))
    # TODO - будут ли сектора
    # TODO - лечение и помощь?
    # TODO - make table substations
    substantion = db.Column(db.Integer())
    place_id = ReferenceCol('places', index=True, nullable=True)
    place = relationship('Place', remote_side="Place.id", foreign_keys=[place_id])
    # TODO - договориться со Смирновым насчет формата адреса
    street = db.Column(db.String(255))
    house = db.Column(db.String(10))
    korpus = db.Column(db.String(10))
    flat = db.Column(db.String(10))
    entrance = db.Column(db.Integer())
    floor = db.Column(db.Integer())
    ent_code = db.Column(db.String(10))
    reason_id = ReferenceCol('reasons', index=True, nullable=True)
    reason = relationship('Reason', remote_side="Reason.id", foreign_keys=[reason_id])
    draft_name = db.Column(db.String(30))
    draft_name2 = db.Column(db.String(30))
    age = db.Column(db.String(30))
    gender = db.Column(db.Integer())

    parent_id = ReferenceCol('calls', index=True, nullable=True)
    parent = relationship('Call',
                          backref=backref('children', cascade="all,delete", order_by="desc(Call.created_at)"),
                          remote_side="Call.id", foreign_keys=[parent_id])

    patient_id = ReferenceCol('patients', index=True, nullable=True)
    patient = relationship('Patient', backref=backref('call'), remote_side="Patient.id", foreign_keys=[patient_id])

    alko = db.Column(db.Boolean(), default=False)
    uvd_fio = db.Column(db.String(30))

    who_call_id = ReferenceCol('who_calls', index=True, nullable=True)
    who_call = relationship('WhoCall', remote_side="WhoCall.id", foreign_keys=[who_call_id])
    who_call_fio = db.Column(db.String(30))
    phone = db.Column(db.String(30))

    first_medic_id = ReferenceCol('users', index=True, nullable=True)
    first_medic = relationship('User', remote_side="User.id", foreign_keys=[first_medic_id])

    second_medic_id = ReferenceCol('users', index=True, nullable=True)
    secind_medic = relationship('User', remote_side="User.id", foreign_keys=[second_medic_id])

    third_medic_id = ReferenceCol('users', index=True, nullable=True)
    third_medic = relationship('User', remote_side="User.id", foreign_keys=[third_medic_id])

    driver_id = ReferenceCol('users', index=True, nullable=True)
    driver = relationship('User', remote_side="User.id", foreign_keys=[driver_id])

    # TODO решить с автомобилями
    automobile = db.Column(db.String(30))

    # TODO - сколько диагнозов и осложнений должно быть

    # Диагноз первичный
    ds_draft = db.Column(db.Integer())
    osl_draft = db.Column(db.Integer())

    ds0 = db.Column(db.String(10))
    # Диагноз основной
    ds1 = db.Column(db.String(10))
    # Диагноз сопутствующего заболевания
    ds2 = db.Column(db.String(10))
    # Диагноз осложнения заболевания
    ds3 = db.Column(db.String(10))
    # TODO - использовать федеральный исход случая
    result = db.Column(db.String(30))

    brg_num = db.Column(db.Integer())
    brg_prof = db.Column(db.String(1))
    brg_subst = db.Column(db.Integer())

    # подается ли вызов в ОМС
    is_oms = db.Column(db.Boolean(), default=True)

    # Признак исправленной записи
    pr_nov = db.Column(db.Boolean(), default=False)

    is_tlt = db.Column(db.Boolean(), default=False)

    invalid = db.Column(db.Integer())

    # F014; 1 - экстренная, 2 - неотложная
    for_pom = db.Column(db.Integer())

    # V012; 401 - без эффекта, 402 - улучшение, 403 - ухудшение
    ishod = db.Column(db.Integer())

    oms_status_id = ReferenceCol('oms_statuses', index=True, nullable=True)
    oms_status = relationship('OmsStatus', backref=backref('call'), remote_side="OmsStatus.id", foreign_keys=[oms_status_id])

    ss_status = db.Column(db.String(), default="draft")

    # Код способа оплаты медицинской помощи V010 (24, 35, 36)
    idsp = db.Column(db.Integer())
    sumv = db.Column(db.Numeric())
    sump = db.Column(db.Numeric())
    sank_it = db.Column(db.Numeric())

    # Тип оплаты: 0 – не принято решение об оплате; 1 – полная; 2 – полный отказ; 3 – частичный отказ.
    oplata = db.Column(db.Integer())


    def __init__(self, **kwargs):
        super(Call, self).__init__(**kwargs)

    def as_dict(self):
        model = self.as_dict_glob()
        return model

    def os_sluch(self):
        if not self.patient.otch or self.patient.otch == "НЕТ":
            return True
        return False


    def __repr__(self):
        """Represent instance as a unique string."""
        return '<Call ({year_id!r})>'.format(tab_id=self.year_id)