# -*- coding: utf-8 -*-
"""User views."""
from flask import Blueprint, render_template, jsonify
from flask_login import login_required
from smp_cms.user.models import User
from smp_cms.utils.view_utils import get_user_by_id

blueprint = Blueprint('user', __name__, static_folder='../static')


@blueprint.route('/users/<id>')
@login_required
@get_user_by_id
def get(user):
    return render_template('users/user_info.html', user_data=user.as_dict())
