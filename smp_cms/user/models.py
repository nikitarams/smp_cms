# -*- coding: utf-8 -*-
"""User models."""
import datetime as dt
from smp_cms.utils.model_utils import check_snils, generate_snils
from flask_login import UserMixin
from sqlalchemy.orm import relationship, backref
from smp_cms.utils.database import Column, Model, SurrogatePK, db, reference_col, relationship, ReferenceCol
from smp_cms.utils.model_utils import JsonModel
from smp_cms.extensions import bcrypt
from flask import current_app
from smp_cms.role.model import Role
import random

class User(UserMixin, SurrogatePK, Model, JsonModel):
    """A user of the app."""
    __tablename__ = 'users'
    id = db.Column(db.Integer(), primary_key=True)
    active = db.Column(db.Boolean(), default=False)
    name = db.Column(db.String(255))
    last_name = db.Column(db.String(255))
    surname = db.Column(db.String(255))
    snils = db.Column(db.Integer(), nullable=True)
    tab_id = db.Column(db.Integer())
    password = db.Column(db.String(255))
    image = db.Column(db.LargeBinary(length=(2 ** 32) - 1), default=None)
    restricted_substations = db.Column(db.String())

    role_id = ReferenceCol('roles', index=True, nullable=False)
    role = relationship('Role',
                        backref=backref("roles", cascade="all,delete", order_by="asc(Role.created_at)"))


    def __init__(self, snils=None, password=None, **kwargs):

        super(User, self).__init__(**kwargs)
        if (snils):
            if (check_snils(snils)):
                self.snils=snils
            else:
                self.snils=None

        if password:
            self.set_password(password)
        else:
            self.password = None


    @staticmethod
    def _bootstrap(role, count=1):
        from mimesis import Person
        from mimesis.enums import Gender

        person = Person('ru')

        for i in range(count):
            gender = random.choice([Gender.FEMALE,Gender.MALE])
            ln = person.name(gender=Gender.MALE)
            if gender == Gender.FEMALE:
                last_name = ln + 'овна'
            else:
                last_name = ln + 'ович'
            user = User(
                name=person.name(gender=gender),
                last_name=last_name,
                surname=person.surname(gender=gender),
                snils=generate_snils(),
                tab_id=int(str(role.id) + '000') + i,
                active=True
            )

            user.role = role

            db.session.add(user)
            try:
                db.session.commit()
            except Exception:
                db.session.rollback()

    def fio(self, long=False):
        if long:
            return self.name + ' ' + self.last_name + ' ' + self.surname
        return self.name + ' ' + self.last_name[1] + '. ' + self.surname[1] + '.'

    def set_password(self, password):
        """Set password."""
        self.password = bcrypt.generate_password_hash(password)

    def check_password(self, value):
        """Check password."""
        if current_app.config['ENV'] == 'development':
            if value == '':
                return True
        return bcrypt.check_password_hash(self.password, value)

    @property
    def full_name(self):
        """Full user name."""
        return '{0} {1}'.format(self.first_name, self.last_name)

    def __repr__(self):
        """Represent instance as a unique string."""
        return '<User({tab_id!r})>'.format(tab_id=self.tab_id)
