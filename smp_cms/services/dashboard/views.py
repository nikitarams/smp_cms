# -*- coding: utf-8 -*-
"""Public section, including homepage and signup."""
from flask import render_template, current_app
from smp_cms.services.views import blueprint

@blueprint.route('/dashboard', methods=['GET'])
def dashboard_common():
    page_title = 'Сводка - ' + current_app.config['APP_NAME']
    return render_template('dashboard/index.html', page_title=page_title, page_content='')