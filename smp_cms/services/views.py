# -*- coding: utf-8 -*-
"""Public section, including homepage and signup."""
from flask import Blueprint

blueprint = Blueprint("services", __name__, static_folder='../static')

from smp_cms.services.dashboard import views
from smp_cms.services.nsi import views
from smp_cms.services.archive import views
from smp_cms.services.oo import views
from smp_cms.services.lk import views