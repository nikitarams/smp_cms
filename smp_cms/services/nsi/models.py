# -*- coding: utf-8 -*-
"""User models."""
from smp_cms.utils.database import Column, Model, SurrogatePK, db, reference_col, relationship, ReferenceCol
from smp_cms.utils.model_utils import JsonModel


class Settings(SurrogatePK, Model, JsonModel):
    """A user of the app."""
    __tablename__ = 'settings'

    @property
    def nsi_file(self):
        return ''

    mo_title = db.Column(db.String(255))
    mo_title_full = db.Column(db.String(255))
    mo_inn = db.Column(db.Integer())
    mo_kpp = db.Column(db.Integer())
    mo_director = db.Column(db.String())
    mo_glavbuh = db.Column(db.String())
    reestr_version = db.Column(db.String())
    mo_reestr_code = db.Column(db.String())

    def __init__(self, **kwargs):
        super(Settings, self).__init__(**kwargs)

    def __repr__(self):
        """Represent instance as a unique string."""
        return '<User({tab_id!r})>'.format(tab_id=self.tab_id)
