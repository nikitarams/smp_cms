# -*- coding: utf-8 -*-
"""Public section, including homepage and signup."""
from flask import render_template, current_app
from smp_cms.services.views import blueprint

@blueprint.route('/nsi', methods=['GET'])
def archive_common():
    page_title = 'Архив - ' + current_app.config['APP_NAME']
    return render_template('archive/index.html', page_title=page_title, page_content='')