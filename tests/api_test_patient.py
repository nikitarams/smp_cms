import requests
from flask import jsonify
from mimesis import Person, Datetime
from mimesis.enums import Gender
from smp_cms.utils.model_utils import generate_oms
from smp_cms.utils.view_utils import random_with_N_digits
from smp_cms.patient.model import *
from smp_cms.user.models import *
from sqlalchemy.sql.expression import func

person = Person('ru')

def generate_patient(parent_id=None, set_gender=None, birthday=None,
                     is_ln=True, is_name=True, is_sn=True,
                     is_snils=True, is_bday=True, is_death=False,
                     is_polis=True, is_docs=True):

    person = Person('ru')

    gen_patient = {}

    if not set_gender:
        gender = random.choice([Gender.FEMALE, Gender.MALE])
    else:
        gender = Gender(set_gender)

    if gender.value == 'female':
        gen_patient['gender'] = 'Ж'
    elif gender.value == 'male':
        gen_patient['gender'] = 'М'

    if is_ln:
        ln = person.name(gender=Gender.MALE)
        if gender == Gender.FEMALE:
            gen_patient['last_name'] = ln + 'овна'
        else:
            gen_patient['last_name'] = ln + 'ович'

    if is_name:
        gen_patient['name'] = person.name(gender=gender)

    if is_sn:
        gen_patient['surname'] = person.surname(gender=gender)

    if is_snils:
        gen_patient['snils'] = generate_snils()

    if is_bday:
        if (birthday):
            gen_patient['birthday'] = str(birthday)
        else:
            gen_patient['birthday'] = str(Datetime().date(start=1930, end=2019))

    if is_death:
        gen_patient['deathday'] = True

    gen_polis = {}
    if is_polis:
        gen_polis['number'] = generate_oms()
        gen_polis['policy_type_id'] = PolicyType().query.order_by(func.random()).first().id
        gen_polis['smo_id'] = Smo().query.order_by(func.random()).first().id

    gen_docs = {}
    if is_docs:
        gen_docs['number'] = random_with_N_digits(6)
        gen_docs['prefix'] = random_with_N_digits(4)
        gen_docs['document_type_id'] = DocumentType().query.order_by(func.random()).first().id

    if parent_id:
        parent = Patient().query.filter_by(id=parent_id).first()
        if parent:
            gen_patient['parent_id'] = parent.id

    res = requests.post(url='http://localhost:5000/api/patients',
                        json=jsonify(patient=gen_patient, document=gen_docs, policy=gen_polis).json)

    return res
