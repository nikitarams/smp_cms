import requests, random
from datetime import datetime
from flask import url_for, current_app, jsonify, json
from mimesis import Person, Datetime, Address, Transport
from mimesis.enums import Gender
from smp_cms.patient.model import *
from smp_cms.call.model import WhoCall, Place, Reason, Result, Profile, Mkb10
from smp_cms.user.models import *
from sqlalchemy.sql.expression import func, select

person = Person('ru')


def generate_call(set_gender=None,
                  is_pat=True, is_name=True, is_sn=True,
                     is_snils=True, is_bday=True, is_death=False,
                     is_polis=True, is_docs=True):

    address = Address('ru')

    call = {}
    call['profile_id'] = Profile().query.order_by(func.random()).first().id
    call['protocol'] = "Протокол вызова СМП"
    call['comment'] = ""
    call['substantion'] = 1
    call['place_id'] = Place().query.order_by(func.random()).first().id
    call['street'] = address.street_name()
    call['house'] = random.randint(1,100)
    call['korpus'] = random.randint(1,100)
    call['flat'] = random.randint(1,100)
    call['entrance'] = random.randint(1,10)
    call['floor'] = random.randint(1,10)
    call['ent_code'] = random.randint(100,999)
    call['reason_id'] = Reason().query.order_by(func.random()).first().id



    gender = random.choice([Gender.FEMALE, Gender.MALE])
    if gender.value == 'female':
        call['gender'] = 'Ж'
    elif gender.value == 'male':
        call['gender'] = 'М'

    if is_pat:
        call['draft_name'] = person.name(gender=gender)
        call['draft_name2'] = random.choice(RUS_LETTERS) + ' ' + random.choice(RUS_LETTERS)

    call['age'] = random.randint(0,99)
    call['patient_id'] = None
    call['alko'] = random.choice([True, False])
    call['uvd_fio'] = person.name()
    call['who_call_id'] = WhoCall().query.order_by(func.random()).first().id
    call['who_call_fio'] = person.full_name()
    call['phone'] = person.telephone()
    call['first_medic_id'] = User().query.order_by(func.random()).first().id
    call['second_medic_id'] = User().query.order_by(func.random()).first().id
    call['third_medic_id'] = User().query.order_by(func.random()).first().id
    call['driver_id'] = User().query.order_by(func.random()).first().id
    call['automobile'] = Transport().car() + ' ' + Transport().vehicle_registration_code()
    call['ds0'] = Mkb10().query.order_by(func.random()).first().code
    call['ds1'] = Mkb10().query.order_by(func.random()).first().code
    call['ds2'] = Mkb10().query.order_by(func.random()).first().code
    call['ds3'] = Mkb10().query.order_by(func.random()).first().code
    call['result'] = Result().query.order_by(func.random()).first().id
    call['brg_num'] = random.randint(100,700)
    call['brg_prof'] = Profile().query.order_by(func.random()).first().id
    call['brg_subst'] = 1
    call['is_oms'] = True
    call['pr_nov'] = False
    call['is_tlt'] = random.choice([True, False])
    call['invalid'] = 0
    call['for_pom'] = random.randint(1,2)
    call['ishod'] = random.choice([401,402,403])
    call['oms_status_id'] = 0
    call['idsp'] = 24
    call['sumv'] = 100
    call['sump'] = 0
    call['sank_it'] = 0
    call['oplata'] = 0
    call['ds_draft'] = random.randint(1,162)
    call['osl_draft'] = random.randint(200,250)

    res = requests.post(url='http://localhost:5000/api/calls',
                        json=jsonify(call=call).json)

    return res
