# -*- coding: utf-8 -*-
"""Create an application instance."""
from smp_cms.app import create_app

app = create_app()
